/* Dijkstra's Algorithm in C */
#include<stdio.h>
#include<string.h>
#include<math.h>
#define IN 99
// #define N 6
#define NRS 6
int dijkstra(int ** cost, int source, int target, int N);
int main()
{
    int ** cost,i,j,w,ch,co;
    
    cost = (int **)malloc(sizeof(int *) * NRS);
    for(i=0; i<NRS; i++){
        cost[i] = (int *)malloc(NRS* sizeof(int));
        int j;
        for(j=0; j<NRS; j++){
            
            cost[i][j] = IN;
        }
    }
    
    int source, target,x,y;
    printf("\t The Shortest Path Algorithm ( DIJKSTRA'S ALGORITHM in C \n\n");
    // for(i=0;i< NRS;i++)
    // for(j=0;j< NRS;j++)
    // cost[i][j] = IN;
    
    /*
    for(x=1;x< N;x++)
    {
        for(y=x+1;y< N;y++)
        {
            printf("Enter the weight of the path between nodes %d and %d: ",x,y);
            scanf("%d",&w);
            cost [x][y] = cost[y][x] = w;
        }
        printf("\n");
    }*/
    
    // !!! COSTUL TREBUIE SA FIE SIMETRIC
    // cost[1][2] = 10;
    // cost[1][3] = 12;
    // cost[2][4] = 20;
    // cost[3][5] = 3;
    // cost[1][5] = 80;
    
    // !!! COSTUL TREBUIE SA FIE SIMETRIC
    cost[0][1] = 10;
    cost[1][0] = 10;
    cost[0][2] = 12;
    cost[2][0] = 12;
    cost[1][3] = 20;
    cost[3][1] = 20;
    cost[2][4] = 3;
    cost[4][2] = 3;
    cost[0][4] = 80;
    cost[4][0] = 80;
    
    printf("\nEnter the source:");
    // scanf("%d", &source);
    source = 1;
    // printf("\nEnter the target");
    // scanf("%d", &target);
    target = 4;
    co = dijsktra(cost,source,target, NRS);
    printf("\nThe Shortest Path: %d",co);
}
int dijsktra(int **cost,int source,int target, int N)
{
    int i,m,min,start,d,j;
    // dist[N]
    // prev[N]
    // selected[N]={0}
    int * dist = (int *)malloc(sizeof(int) * N);
    int * prev = (int *)malloc(sizeof(int) * N);
    int * selected = (int *)malloc(sizeof(int) * N);
    for(i=0; i<N; i++){
        selected[i] = 0;
    }
    
    char path[N];
    for(i=0;i< N;i++)
    {
        dist[i] = IN;
        prev[i] = -1;
    }
    start = source;
    selected[start]=1;
    dist[start] = 0;
    while(selected[target] ==0)
    {
        min = IN;
        m = 0;
        for(i=0;i< N;i++)
        {
            d = dist[start] +cost[start][i];
            if(d< dist[i]&&selected[i]==0)
            {
                dist[i] = d;
                prev[i] = start;
            }
            if(min>dist[i] && selected[i]==0)
            {
                min = dist[i];
                m = i;
            }
        }
        start = m;
        selected[start] = 1;
    }
    start = target;
    j = 0;
    while(start != -1)
    {
        printf("PATH ELEMENT: %d\n", start);
        path[j++] = start+65;
        start = prev[start];
    }
    path[j]='\0';
    // strrev(path);
    printf("%s", path);
    return dist[target];
}