#include <iostream>

using namespace std;

typedef struct{
    // n - numar noduri
    // m - numar arce (muchii)
    int n,m;
    int ** a; // adresa matrice de adiacenta
    
}Graf;

// functie initializare graf
void initG(Graf &g, int n){
    int i;
    g.n = n; 
    g.m = 0;
    g.a = (int**)malloc((n+1)*sizeof(int*));
    // varfuri numerotate 1..n
    for(i=1; i<=n; i++){ // linia 0 si coloana 0 nu sunt folosite
        g.a[i] = (int*)calloc((n+1), sizeof(int));
        
    }
}

// functie adaugare arc (x,y) la graf g
void addArc(Graf &g, int x, int y){
    g.a[x][y] = 1;
    g.m++;
}

// functie care intoarce arc (x,y) din graf g
int arc(Graf g, int ix, int y){
    return g.a[x][y];
}

// functie eliminare arc (x,y)
void delArc(Graf &g, int x, int y){
    g.a[x][y] = 0;
    g.m--;
}

// eliminare nod x din graf g
void delNod(Graf &g, int x){
    int i;
    for(i=1; i<=g.n; i++){
        delArc(g, x, i);
        delArc(g, i, x);
    }
}

int main(){
    
    return 0;
}