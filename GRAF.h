#include <stdio.h>
#include <stdlib.h>
#include <string.h>


# define M 20 // nr maxim de noduri
# define M1 10000 // un nr f mare (cost arc absent)
# define M2 (M1+1) // alt nr f mare (cost arc folosit)
# define MARE 23000

typedef struct{
    int n,m; // noduri si arce
    int ** c; // matrice ponderi (costuri)
}GrafP;

// funcție de inițializare a grafului
void initG (GrafP * g, int n) { 
    int i;
    g->n=n;
    g->m = 0;
    g->c = (int**) malloc( (n)*sizeof(int*));
    // vârfuri numerotate 1...n
    for (i = 0; i < n; i++)
        g->c[i] = (int*) calloc( (n), sizeof(int));
    // linia 0 și coloana 0 sunt nefolosite
}

 
// adaugare arc (v,w)
void addArc(GrafP * g, int v, int w, int cost){
    g->c[v][w] = cost;
    g->m++;
}

// intoarce cost arc (v,w)
int costArc(GrafP g, int v, int w){
    return g.c[v][w];
}


void afisareGraf(GrafP * gr){
    printf("------------Graf---------------\n");
    printf("\t\tNoduri: %d\n\t\tArce: %d\n", gr->n, gr->m);
    printf("\t\tMatrice adiacenta:\n");
    printareMatrice(gr->c, gr->n);
}





/**
Pentru memorarea nodurilor de pe un drum minim se folosește un tablou P,
cu p[i] = nodul precedent lui i pe drumul minim de la 1 la i 
(mulțimea drumurilor minime formează un arbore, iar tabloul P 
    reprezintă acest arbore de căi în graf)
*/
void dijkstra (GrafP g,int p[]) {
    int d[M], s[M]; // d - lungimile drumurilor minime d[2], d[3],..., d[n] până la nodurile 2, 3,..., n
                  // s = noduri pentru care se știe distanța minimă
    printf("P1\n"); int q;
    int dmin, jmin;
    int i, j;
    for (i = 1; i <= g.n; i++) {
        p[i]=0; d[i]=costArc(g, 0, i);
        // distanțe inițiale de la 1 la alte noduri
    }
    printf("P2\n");
    s[0] = 1;
    for (i = 1; i < g.n; i++) { // repetă de n-1 ori
        // caută nodul j pentru care d[j] este minim
        dmin = MARE;
        for ( j = 1; j < g.n; j++)
            // determină minimul dintre distanțele d[j]
            if (s[j] == 0 && dmin > d[j]) {
                // dacă j  S și este mai aproape de S
                dmin = d[j]; jmin = j;
            }
        s[jmin] = 1; // adaugă nodul jmin la S
        for ( j = 1; j < g.n; j++)
            // recalculare distanțe noduri față de 1
            if ( d[j] > d[jmin] + costArc(g, jmin, j) ) {
                d[j] = d[jmin] + costArc(g, jmin, j);
                p[j] = jmin;
                // predecesorul lui j pe drumul minim
            }
    }
    printf("P3\n");
   
    for(q=0; q<g.n; q++){
        printf("<%d-%d>", q, d[q]);
    }
    printf("\n");
}


/**
Pentru memorarea nodurilor de pe un drum minim se folosește un tablou P, cu p[i] = nodul precedent lui i pe drumul minim de la 1 la i (mulțimea drumurilor minime formează un arbore, iar tabloul P reprezintă acest arbore de căi în graf)
*/
void dijkstra2 (GrafP g,int p[]) {
    printf("1001\n");
    afisareGraf(&g);
    printf("1002\n");
    int d[M], s[M]; // d - lungimile drumurilor minime d[2], d[3],..., d[n] până la nodurile 2, 3,..., n
                  // s = noduri pentru care se știe distanța minimă
    int dmin, jmin;
    int i, j;
    for (i = 1; i < g.n; i++) {
        p[i]=0; d[i]=costArc(g, 0, i);
        // distanțe inițiale de la 0 la alte noduri
    }
    s[0] = 1;
    for (i = 2; i <= g.n; i++) { // repetă de n-1 ori
        // caută nodul j pentru care d[j] este minim
        dmin = MARE;
        for ( j = 2; j <= g.n; j++)
            // determină minimul dintre distanțele d[j]
            if (s[j] == 0 && dmin > d[j]) { // exp s[0] = 1
                // dacă j  S și este mai aproape de S
                dmin = d[j]; jmin = j;
            }
        s[jmin] = 1; // adaugă nodul jmin la S
        for ( j = 1; j < g.n; j++)
            // recalculare distanțe noduri față de 1
            if ( d[j] > d[jmin] + costArc(g, jmin, j) ) {
                d[j] = d[jmin] + costArc(g, jmin, j);
                p[j] = jmin;
                // predecesorul lui j pe drumul minim
            }
    }
    // debug
    int q;
    for(q=0; q<g.n; q++){
        printf("<%d>", p[q]);
    }
    printf("\n");
}

