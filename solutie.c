#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "UTIL.h"
// #include "DIJKSTRA.h"
#include "RETEA.h"

#define IN 9999
// 99

// TODO: clear memory
//      clear warnings


int** getMatriceCosturi(struct Topologie * top){
    int ** c; // matrice ponderi
    c = (int **)malloc(sizeof(int *) * top->numarSwitchuri); // n switch-uri 
    int i;
    for(i=0; i<top->numarSwitchuri; i++){
        c[i] = (int *)malloc(top->numarSwitchuri* sizeof(int));
        int j;
        for(j=0; j<top->numarSwitchuri; j++){
            // c[i][j] = 0; // TODO: aici ar trebui sa fie INFINIT!!!
            c[i][j] = IN;
        }
    }
    for(i=0; i<top->numarLegaturi; i++){
        struct Legatura * tempLegatura = top->legaturi[i];
        // printf("c[%d][%d]=%d\n", tempLegatura->switchSursa,tempLegatura->switchDestinatie, calculeazaCost(tempLegatura->viteza));
        c[tempLegatura->switchSursa][tempLegatura->switchDestinatie] = calculeazaCost(tempLegatura->viteza);
        // !!! COSTUL TREBUIE SA FIE SIMETRIC
        c[tempLegatura->switchDestinatie][tempLegatura->switchSursa] = calculeazaCost(tempLegatura->viteza);
    }
    // printareMatrice(c, top->numarSwitchuri);
    return c;
}







/**
 * 
 Functie care ne da un Bridge ID
 */
struct Switch * findSwitchWithLowerBridgeID(struct Switch * sw1, struct Switch * sw2){
    if(sw1->prioritate < sw2->prioritate){
        return sw1;
    }else if(sw1->prioritate > sw2->prioritate){
        return sw2;
    }
    // prioritatile sunt egale
    char * macLetter1 = sw1->MAC;
    char * macLetter2 = sw2->MAC;
    while(*macLetter1){
        int mc1Int = (int)macLetter1;
        int mc2Int = (int)macLetter2;
        if(mc1Int < mc2Int){
            return sw1;
        }
        
        macLetter1++;
        macLetter2++;
    }
    return NULL;
}

/**
 Merge prin retea
*/
struct Switch * findRootBridge(struct Network* net){
    struct Switch * minimumSwitch = net->switches[0];
    int i;
    for(i=1; i<net->numberOfSwitches-1; i++){ // merge prin retea
        struct Switch * currentSwitch = findSwitchWithLowerBridgeID(net->switches[i], net->switches[i+1]);
        printSwitch(currentSwitch);
        minimumSwitch = findSwitchWithLowerBridgeID(minimumSwitch, currentSwitch);
        
    }
    return minimumSwitch;
}




typedef struct Path{
    int to;
    int from;
    int * nodes;
    int distance;
    int numberOfNodes;
} Path;

void printPath(Path * p){
    printf("---PATH::: FROM: %d TO %d-----------------------\n", p->from, p->to);
    printf("\t\tNumber of nodes: %d\n", p->numberOfNodes);
    printf("\t\tNODES: ");
    int i;
    for(i=0; i<p->numberOfNodes; i++){
        printf("%d - ", p->nodes[i]);
    }
    printf("\n");
    printf("\t\tDistance: %d\n", p->distance);
    printf("------------------------------------------------\n");
    
}


// --- /functii

Path * dijsktraOK(int **cost,int source,int target, int N)
{
    int i,m,min,start,d,j;

    int * dist = (int *)malloc(sizeof(int) * N);
    int * prev = (int *)malloc(sizeof(int) * N);
    int * selected = (int *)malloc(sizeof(int) * N);
    for(i=0; i<N; i++){
        selected[i] = 0;
    }
    
    for(i=0;i< N;i++)
    {
        dist[i] = IN;
        prev[i] = -1;
    }
    start = source;
    selected[start]=1;
    dist[start] = 0;
    while(selected[target] ==0)
    {
        min = IN;
        m = 0;
        for(i=0;i< N;i++)
        {
            d = dist[start] +cost[start][i];
            if(d< dist[i]&&selected[i]==0)
            {
                dist[i] = d;
                prev[i] = start;
            }
            if(min>dist[i] && selected[i]==0)
            {
                min = dist[i];
                m = i;
            }
        }
        start = m;
        selected[start] = 1;
    }
    start = target;
    j = 0;
    Path * pathS = (Path *)malloc(sizeof(Path));
    pathS->from = source;
    pathS->to = target;
    pathS->nodes = (int *)malloc(sizeof(int) * N);
    while(start != -1)
    {
        // printf("PATH ELEMENT: %d %d\n", start, j);
        pathS->nodes[j] = start;
        pathS->numberOfNodes++;
        j++;
        start = prev[start];
    }
    pathS->numberOfNodes = j;
    reverse_array(pathS->nodes, pathS->numberOfNodes);
    pathS->distance = dist[target];
    // printPath(pathS);
    return pathS; // dist[target];
}



int** getMatriceTopologie(struct Topologie * top){
    int ** c; // matrice ponderi
    c = (int **)malloc(sizeof(int *) * top->numarSwitchuri); // n switch-uri 
    int i;
    for(i=0; i<top->numarSwitchuri; i++){
        c[i] = (int *)malloc(top->numarSwitchuri* sizeof(int));
        int j;
        for(j=0; j<top->numarSwitchuri; j++){
            c[i][j] = 0;
        }
    }
    for(i=0; i<top->numarLegaturi; i++){
        struct Legatura * tempLegatura = top->legaturi[i];
        if(tempLegatura->portDestinatieEsteRootPort){
            // c[tempLegatura->switchSursa][tempLegatura->switchDestinatie] = 1;
            c[tempLegatura->switchDestinatie][tempLegatura->switchSursa] = 1;
        }
    }
    printareMatrice(c, top->numarSwitchuri);
    return c;
}


void restructurare(struct Network * retea, struct Topologie * top){
    struct Switch * rootBridge = findRootBridge(retea);
    top->numarSwitchuri = retea->numberOfSwitches;   
      int **matriceCosturi = getMatriceCosturi(top);
   
    printf("--\n");
    int p[top->numarSwitchuri];
    
    // ------------------------------------- RP, DP, BP - START
    printf("--------------------calcul ROOT PORTS-----------------\n\n");
    int idRootBridge = rootBridge->id; // id-ul root bridge-ului
    struct Switch ** switchuri = retea->switches;
    int i;
    for(i=0; i<retea->numberOfSwitches; i++){
        // printSwitch(switchuri[i]);
        int switchID = switchuri[i]->id;
        printf("SWITCH ID: %d\n", switchID);
        // avem switch-id-urile, calculam root port-urile din topologie cu dijkstra
        if(switchID != idRootBridge){
            // dijkstra pentru FIECARE switch
            Path * cale = dijsktraOK(matriceCosturi, switchID, idRootBridge, top->numarSwitchuri);
                        
            int * noduri = cale->nodes;
            sortareArray(noduri, cale->numberOfNodes);
            printf("VECTOR NODURI DIN DRUM:\n");
            printArray(noduri, cale->numberOfNodes);
            
            
            // luam elementele 2 cate 2 
            int j;
            for(j=0; j<cale->numberOfNodes-1; j++){
                printf("PERECHE: %d-%d\n", noduri[j], noduri[j+1]);
                struct Legatura * leg1 = getLegaturaDupaSursaSiDestinatie(top, noduri[j], noduri[j+1]);
                printf("LEGATURA CARE SE STABILESTE ESTE:\n");
                leg1->portDestinatieEsteRootPort = 1;
                printLegatura(leg1);
            }
            printPath(cale);
        }
    }
    
     // În primul rând marcăm toate porturile root-bridge-ului ca fiind DP.
    for(i=0; i< top->numarLegaturi; i++){
        struct Legatura * legaturaCurenta = top->legaturi[i];
        if(legaturaCurenta->switchSursa == idRootBridge){
            legaturaCurenta->portSursaEsteDesignatedPort = 1;
        }
    }
    
    // Apoi peste tot pe unde am marcat un RP, marcăm în partea opusă a muchiei DP.
    for(i=0; i< top->numarLegaturi; i++){
        struct Legatura * legaturaCurenta = top->legaturi[i];
        
        // printf("ROOT PORT %d - %d / %d - %d (%d)\n", legaturaCurenta->switchDestinatie, legaturaCurenta->switchSursa, legaturaCurenta->portDestinatie, legaturaCurenta->portSursa, legaturaCurenta->portDestinatieEsteRootPort );
        if(legaturaCurenta->portDestinatieEsteRootPort == 1){
           // OK printf("Pe switch-ul %d portul root %d\n", legaturaCurenta->switchDestinatie, legaturaCurenta->portDestinatie);
           printf("RP: %d(%d)", legaturaCurenta->switchDestinatie, legaturaCurenta->portDestinatie);
           
           // ^ calcul ROOT PORT
           // printf("DP: %d(%d)\n", legaturaCurenta->switchSursa, legaturaCurenta->portSursa->switchDestinatie);
           legaturaCurenta->portSursaEsteDesignatedPort = 1;
        }
        // printLegatura(legaturaCurenta);
    }
    
    // PORTURI RAMASE LIBERE
    printf("\n\nPORTURI LIBERE:\n\n");
    for(i=0; i< top->numarLegaturi; i++){
        struct Legatura * legaturaCurenta = top->legaturi[i];
        if(!legaturaCurenta->portDestinatieEsteRootPort && !legaturaCurenta->portSursaEsteDesignatedPort){
            printf("%d(%d)\n", legaturaCurenta->switchSursa, legaturaCurenta->portSursa);
            
            int idSwitchCurent = legaturaCurenta->switchSursa;
            legaturaCurenta->portSursaEsteDesignatedPort = 1;
        }
    }
}




int main(){
    
    

    struct Network * retea = readInitialize();
    printTheNet(retea);
    
    
    
    printf("\n\n");
    
    printf("\n\n");
    struct Topologie * top = readTopology();
    
    restructurare(retea, top);
    
        
    printf("-------------------------------\n\n");
    
  
    int i;
    
    // DEBUG
    // afisam DESIGNATED PORTS:
    printf("\nDP: ___________________________________________________________\n\n");
    for(i=0; i< top->numarLegaturi; i++){
        struct Legatura * legaturaCurenta = top->legaturi[i];
        // printLegatura(legaturaCurenta);   
        printf("%d(%d)    -- %d\n",legaturaCurenta->switchSursa, legaturaCurenta->portSursa, legaturaCurenta->portSursaEsteDesignatedPort);
    }
   
    printf("\nBP: ___________________________________________________________\n\n");
    for(i=0; i< top->numarLegaturi; i++){
        struct Legatura * legaturaCurenta = top->legaturi[i];
        // printLegatura(legaturaCurenta);   
        if(legaturaCurenta->portSursaEsteDesignatedPort && legaturaCurenta->portDestinatieEsteRootPort == 0){
            printf("%d(%d)   \n",legaturaCurenta->switchDestinatie, legaturaCurenta->portDestinatie);
        }
    }
    
    // ------------------------------------- RP, DP, BP - END
   
   
    // --------------------------------------- CERINTA 2.3 - START
    printf("\n\n----------------------------------------------------------------\n\n");
    // printTopologieCERINTA23(top);
    
    // getMatriceTopologie(top), top->numarSwitchuri;
    getMatriceTopologie(top);
    // --------------------------------------- CERINTA 2.3 - END
    
    
    
    // -------------------------------------- CITIRE TASKURI DIN FISIER
    
    
    printf("______________________________________________________________________\n");
    printf("______________________________________________________________________\n");
    printf("______________________________________________________________________\n");
    
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    size_t read;

    fp = fopen("Tasks.in", "r");
    
    FILE * fpOut = fopen("stp.out", "w");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1) {
        printf("\n");
        if(line[1] == '1'){
            int idRootBridge = findRootBridge(retea)->id;
            printf("%d\n", idRootBridge);
            fprintf(fpOut, "%d\n", idRootBridge);
        }else if(line[1] == '2'){
            if(line[3] == '1'){ // c2-1
                // afisare RP
                
                
                int numarRootPorts = 0;
                for(i=0; i< top->numarLegaturi; i++){
                    struct Legatura * legaturaCurenta0 = top->legaturi[i];
                    if(legaturaCurenta0->portDestinatieEsteRootPort == 1){
                        numarRootPorts++;
                    }
                }   
                printf("RP: ");
                fprintf(fpOut, "RP: ");
                int rpCurent = 0;
                for(i=0; i< top->numarLegaturi; i++){
                    struct Legatura * legaturaCurenta = top->legaturi[i];
                    if(legaturaCurenta->portDestinatieEsteRootPort == 1){
                        if(rpCurent==numarRootPorts-1){
                            fprintf(fpOut, "%d(%d)", legaturaCurenta->switchDestinatie, legaturaCurenta->portDestinatie);     
                        }else{
                            fprintf(fpOut, "%d(%d) ", legaturaCurenta->switchDestinatie, legaturaCurenta->portDestinatie);     
                        }
                        rpCurent++;
                       printf("%d(%d) ", legaturaCurenta->switchDestinatie, legaturaCurenta->portDestinatie);
                    }
                }
                
                printf("\n");
                fprintf(fpOut, "\n");
                
            }else if(line[3] == '2'){ // c2-2
                // printf("AFISARE BLOCKED PORTS\n");
                
                    // BLOCKED PORTS
                    int numarBP = 0;
                    for(i=0; i< top->numarLegaturi; i++){
                        struct Legatura * legaturaCurenta = top->legaturi[i];
                        if(legaturaCurenta->portSursaEsteDesignatedPort && legaturaCurenta->portDestinatieEsteRootPort == 0){
                             numarBP++;       
                        }
                        
                    }
                    
                    int nrCurentBP = 0;
                    
                    printf("BP: ");
                    if(numarBP == 0){
                        fprintf(fpOut, "BP:");    
                    }else{
                        fprintf(fpOut, "BP: ");    
                    }
                    
                    for(i=0; i< top->numarLegaturi; i++){
                        struct Legatura * legaturaCurenta = top->legaturi[i];
                        if(legaturaCurenta->portSursaEsteDesignatedPort && legaturaCurenta->portDestinatieEsteRootPort == 0){
                            printf("%d(%d) ",legaturaCurenta->switchDestinatie, legaturaCurenta->portDestinatie);
                            if(nrCurentBP == numarBP-1){
                                fprintf(fpOut, "%d(%d)",legaturaCurenta->switchDestinatie, legaturaCurenta->portDestinatie);    
                            }else{
                                fprintf(fpOut, "%d(%d) ",legaturaCurenta->switchDestinatie, legaturaCurenta->portDestinatie);        
                            }                         
                            nrCurentBP++;
                            
                        }
                    }
                    printf("\n");
                    fprintf(fpOut, "\n");
                
            }else{ // c2-3
               int ** mt = getMatriceTopologie(top); 
               printareMatriceInFisier(mt, top->numarSwitchuri, fpOut);
            }
        }else if(line[1] =='3'){ // c3
            // printf("CALEA MESAJULUI de la %c la %c\n", line[3], line[5]);
            
            int **matriceCosturi2 = getMatriceCosturi(top);
             for(i=0; i< top->numarLegaturi; i++){
                struct Legatura * legaturaCurenta = top->legaturi[i];
                if(legaturaCurenta->portSursaEsteDesignatedPort && legaturaCurenta->portDestinatieEsteRootPort == 0){
                       matriceCosturi2[legaturaCurenta->switchSursa][legaturaCurenta->switchDestinatie] = IN;
                       matriceCosturi2[legaturaCurenta->switchDestinatie][legaturaCurenta->switchSursa] = IN;
                }
             }
            
            Path * cale2 = dijsktraOK(matriceCosturi2, 4, 2, top->numarSwitchuri);
            // printPath(cale2);
            int k;
            for(k=0; k<cale2->numberOfNodes; k++){
                printf("%d ", cale2->nodes[k]);
                if(k==cale2->numberOfNodes-1){
                    fprintf(fpOut, "%d", cale2->nodes[k]);
                }else{
                    fprintf(fpOut, "%d ", cale2->nodes[k]);
                }
            }
            fprintf(fpOut, "\n");
            
    
        }else if(line[1] == '4'){ // c4
            printf("CADERE LEGATURA DINTRE %c si %c\n", line[3], line[5]);
            int idSS = ((int)line[3])-48;
            int idSD = ((int)line[5])-48;
            printf("ID SS : ============================================== %d ======ID DD: %d\n", idSS, idSD);
            removeLegatura(top, idSS, idSD);
            restructurare(retea, top);
            
        }
    }
    
    
    fclose(fp);
    fclose(fpOut);
    if (line)
        free(line);
    // exit(EXIT_SUCCESS);
    // -------------------------------------- CITIRE TASKURI END
    
    printf("--------------CERINTA 3----------------------\n");
    
    
    
    
    /*
    int **matriceCosturi2 = getMatriceCosturi(top);
     for(i=0; i< top->numarLegaturi; i++){
        struct Legatura * legaturaCurenta = top->legaturi[i];
        if(legaturaCurenta->portSursaEsteDesignatedPort && legaturaCurenta->portDestinatieEsteRootPort == 0){
               matriceCosturi2[legaturaCurenta->switchSursa][legaturaCurenta->switchDestinatie] = IN;
               matriceCosturi2[legaturaCurenta->switchDestinatie][legaturaCurenta->switchSursa] = IN;
        }
     }
    // printareMatrice(matriceCosturi2, top->numarSwitchuri);
    
    printf("PATH NOU: ");
    Path * cale2 = dijsktraOK(matriceCosturi2, 4, 2, top->numarSwitchuri);
    printPath(cale2);
    
    */
    return 0;   
}