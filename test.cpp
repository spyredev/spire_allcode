#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int main(void)
{
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;

    fp = fopen("Topology.in", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1) {
        printf("Retrieved line of length %zu :\n", read);
        printf("%s", line);
        
        
        // char input[] = "A bird came down the walk";
        printf("Parsing the input string '%s'\n", line);
        char *token = strtok(line, " ");
        
        int idSwitch1 = atoi(token);
        
        token = strtok(NULL, " ");
        int idSwitch2 = atoi(token);
        token = strtok(NULL, " ");
        int port1 = atoi(token);
        token = strtok(NULL, " ");
        int port2 = atoi(token);
        token = strtok(NULL, " ");
        int cost = atoi(token);
        
        printf("ID sw1: %d, ID Sw2: %d, port 1: %d, port 2: %d, cost: %d\n", idSwitch1, idSwitch2, port1, port2, cost);
        
        // while(token) {
        //     puts(token);
        //     token = strtok(NULL, " ");
        // }
        
    }

    fclose(fp);
    if (line)
        free(line);
    exit(EXIT_SUCCESS);
}