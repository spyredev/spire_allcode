#include <stdio.h>
#include <stdlib.h>
#include <string.h>


void printArray(int * arr, int dim){
    int i;
    printf("ARR[");
    for(i=0; i<dim; i++){
        printf("%d, ", arr[i]);
    }
    printf("]\n");
}

void reverse_array(int *pointer, int n)
{
   int *s, c, d;
 
   s = (int*)malloc(sizeof(int)*n);
 
   if( s == NULL )
      exit(EXIT_FAILURE);
 
   for ( c = n - 1, d = 0 ; c >= 0 ; c--, d++ )
      *(s+d) = *(pointer+c);
 
   for ( c = 0 ; c < n ; c++ )
      *(pointer+c) = *(s+c);
 
   free(s);
}

void printareMatrice(int **mat, int dim){
    int i,j;
    for(i=0; i<dim; i++){
        for(j=0; j<dim; j++){
            printf("%d ", mat[i][j]);
        }
        printf("\n");
    }
}

void printareMatriceInFisier(int **mat, int dim, FILE *pf){
    int i,j;
    for(i=0; i<dim; i++){
        for(j=0; j<dim; j++){
            if(j==dim-1){
                fprintf(pf, "%d", mat[i][j]);
            }else{
                fprintf(pf, "%d ", mat[i][j]);
            }
        }
        fprintf(pf, "\n");
    }
}


void debug(int step){
    printf("[DEBUG STEP %d]\n", step);
}

void debug2(char * message, int val){
    printf("[DEBUG %s - %d]\n", message, val);
}

void sortareArray(int * v, int dim){
    
    int i,j;
    for(i=0; i<dim; i++){
        for(j=0; j<dim; j++){
            if(v[i]<v[j]){
                int temp = v[i];
                v[i] = v[j];
                v[j] = temp;
            }
        }
    }
}
