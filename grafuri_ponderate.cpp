#include <iostream>

using namespace std;

# define M 20 // nr maxim de noduri
# define M1 10000 // un nr f mare (cost arc absent)
# define M2 (M1+1) // alt nr f mare (cost arc folosit)
# define MARE 23000

typedef struct{
    int n,m; // noduri si arce
    int ** c; // matrice ponderi (costuri)
}GrafP;

// adaugare arc (v,w)
void addArc(GrafP & g, int v, int w, int cost){
    g.c[v][w] = cost;
    g.m++;
}

// intoarce cost arc (v,w)
int costArc(GrafP g, int v, int w){
    return g.c[v][w];
}

// funcție de inițializare a grafului
void initG (GrafP & g, int n) { 
    int i;
    g.n=n;
    g.m = 0;
    g.c = (int**) malloc( (n+1)*sizeof(int*));
    // vârfuri numerotate 1...n
    for (i = 1; i <= n; i++)
    g.c[i] = (int*) calloc( (n+1), sizeof(int));
    // linia 0 și coloana 0 sunt nefolosite
}

/**
Pentru memorarea nodurilor de pe un drum minim se folosește un tablou P, cu p[i] = nodul precedent lui i pe drumul minim de la 1 la i (mulțimea drumurilor minime formează un arbore, iar tabloul P reprezintă acest arbore de căi în graf)
*/
void dijkstra (GrafP g,int p[]) {
    int d[M], s[M]; // d - lungimile drumurilor minime d[2], d[3],..., d[n] până la nodurile 2, 3,..., n
                  // s = noduri pentru care se știe distanța minimă
    int dmin, jmin;
    int i, j;
    for (i = 2; i <= g.n; i++) {
        p[i]=1; d[i]=costArc(g, 1, i);
        // distanțe inițiale de la 1 la alte noduri
    }
    s[1] = 1;
    for (i = 2; i <= g.n; i++) { // repetă de n-1 ori
        // caută nodul j pentru care d[j] este minim
        dmin = MARE;
        for ( j = 2; j <= g.n; j++)
            // determină minimul dintre distanțele d[j]
            if (s[j] == 0 && dmin > d[j]) {
                // dacă j  S și este mai aproape de S
                dmin = d[j]; jmin = j;
            }
        s[jmin] = 1; // adaugă nodul jmin la S
        for ( j = 2; j <= g.n; j++)
            // recalculare distanțe noduri față de 1
            if ( d[j] > d[jmin] + costArc(g, jmin, j) ) {
                d[j] = d[jmin] + costArc(g, jmin, j);
                p[j] = jmin;
                // predecesorul lui j pe drumul minim
            }
            
    }
    int q;
     for(q=1; q<=g.n; q++){
        printf("<%d>", p[q]);
    }
    printf("\n\n");
}


int main(){
    
    GrafP retea;
    initG(retea, 5);
    retea.n = 5;
    retea.m = 5;
    
    addArc(retea, 1, 2, 4);
    addArc(retea, 1, 4, 4);
    addArc(retea, 2, 3, 19);
    addArc(retea, 3, 4, 100);
    addArc(retea, 4, 5, 1);
    addArc(retea, 2, 5, 19);
    
    int p[10];
    int i;
    for(i=0; i<10; i++){
        p[i] = 0;
    }
    
    dijkstra(retea, p);
    return 0;   
}