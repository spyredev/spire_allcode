#include <iostream>

using namespace std;

typedef struct nod{
    int val; // numar nod
    struct nod * leg; // adresa listei de succesori pt un nod
}* pnod;

typedef struct{
    int n; // numar de noduri in graf
    pnod * v; // tablou de pointeri la liste de succesori
}Graf;


// initializare
void initG(Graf &g, int n){
    g.n = n; 
    g.v = (pnod*)calloc(n+1, sizeof(pnod)); // initializare pointeri cu 0
}

// adaugare arc
void addArc(Graf &g, int x, int y){
    pnod nou = (pnod) malloc(sizeof(nod));
    nou->val = y;
    nou->leg = g.v[x];
    g.v[x] = nou; // se adauga la inceputul listei de adiacenta
}

// functie care testeaza daca exista arcul (x,y) in graful g
int arc(Graf g, int x, int y){
    pnod p;
    for(p=g.v[x]; p!= NULL; p=p->leg)
        if(y==p->val)
         return 1;
    return 0;
}

int main(){
    
    return 0;
}