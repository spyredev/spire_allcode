#include <stdio.h>
#include <stdlib.h>
#include <string.h>



int main(void)
{
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    size_t read;

    fp = fopen("Tasks.in", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);

    while ((read = getline(&line, &len, fp)) != -1) {
        // printf("%s", line);
        
        if(line[1] == '1'){
            printf("Stabilire root bridge\n");
        }else if(line[1] == '2'){
            if(line[3] == '1'){ // c2-1
                printf("AFISARE ROOT PORTS\n");
            }else if(line[3] == '2'){ // c2-2
                printf("AFISARE BLOCKED PORTS\n");
            }else{ // c2-3
                printf("AFISARE TOPOLOGIE\n");
            }
        }else if(line[1] =='3'){ // c3
            printf("CALEA MESAJULUI de la %c la %c\n", line[3], line[5]);
    
        }else if(line[1] == '4'){ // c4
            printf("CADERE LEGATURA DINTRE %c si %c\n", line[3], line[5]);
        }
    }

    fclose(fp);
    if (line)
        free(line);
    exit(EXIT_SUCCESS);
}