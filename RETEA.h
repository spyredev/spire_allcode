#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int calculeazaCost(int viteza){
    if(viteza == 10)
        return 100;
    if(viteza == 100)
        return 19;
    if(viteza == 1000)
        return 4;
    if(viteza == 10000)
        return 1;
    return -9999;
}


struct Switch{
    int id;
    int prioritate;
    char * MAC;
    
};

struct Port{
    int switchId;
    int numarPort;
};

struct Network{
    struct Switch ** switches;
    int numberOfSwitches;
};

//       TOPOLOGIE - start
struct Legatura{
    int switchSursa;
    int switchDestinatie;
    int portSursa;      // DP?
    int portDestinatie; // RP?
    int viteza;
    int portDestinatieEsteRootPort;
    // test
    int portSursaEsteDesignatedPort;
    
};

struct Topologie{
    struct Legatura ** legaturi;
    int numarSwitchuri; // transmis 'artificial'
    int numarLegaturi;
};

void removeLegatura(struct Topologie * top, int ss, int sd){
    int numarLegaturaCeTrebuieScoasa = -1;
    int gasita = 0;
    int index = 0;
    int i;
    for(i=0; i< top->numarLegaturi; i++){
        struct Legatura * legaturaCurenta = top->legaturi[i];
                
                        
        if(legaturaCurenta->switchSursa == ss && legaturaCurenta->switchDestinatie == sd){
            
            
            gasita = 1;
            numarLegaturaCeTrebuieScoasa = index;
            break;
        }
        index++;
    }
    if(gasita == 1){
        for(i=numarLegaturaCeTrebuieScoasa; i< top->numarLegaturi-1; i++){
            top->legaturi[i] = top->legaturi[i+1];
        }
        top->numarLegaturi--;
    }
}

/**
 * Metoda pentru preluarea unei legaturi din topologie
 * In momentul in care parsam graful, dorim sa modificam legaturile
 * (e.g. sa setam daca portul este un port sursa sau nu)
 * @returns NULL daca nu este gasita legatura, obiectul legatura in caz contrar
 */
struct  Legatura * getLegaturaDupaSursaSiDestinatie(struct Topologie * top, int sursa, int destinatie){
    int i;
    for(i=0; i< top->numarLegaturi; i++){
        struct Legatura * legaturaCurenta = top->legaturi[i];
        if(legaturaCurenta->switchSursa == sursa && legaturaCurenta->switchDestinatie == destinatie){
            return legaturaCurenta;
        }        
    }
    return NULL;
}

/**
 * citeste fisierul Topology.in si preia informatiile in memorie
 */
struct Topologie * readTopology(){
    struct Topologie * theTopology = (struct Topologie *)malloc(sizeof(struct Topologie));
    theTopology->numarLegaturi = 0;
    FILE * fp;
    char * line = NULL;
    size_t len = 0;
    size_t read;

    fp = fopen("Topology.in", "r");
    if (fp == NULL)
        exit(EXIT_FAILURE);
    while ((read = getline(&line, &len, fp)) != -1) {
        
        theTopology->numarLegaturi++;
        theTopology->legaturi = (struct Legatura **) realloc(theTopology->legaturi, sizeof (struct Legatura *) * theTopology->numarLegaturi);
        
        printf("Retrieved line of length %zu :\n", read);
        printf("%s", line);
        
        
        // char input[] = "A bird came down the walk";
        printf("Parsing the input string '%s'\n", line);
        char *token = strtok(line, " ");
        
        int idSwitch1 = atoi(token);
        
        token = strtok(NULL, " ");
        int idSwitch2 = atoi(token);
        token = strtok(NULL, " ");
        int port1 = atoi(token);
        token = strtok(NULL, " ");
        int port2 = atoi(token);
        token = strtok(NULL, " ");
        int viteza = atoi(token);
        struct Legatura * legatura = (struct Legatura *)malloc(sizeof(struct Legatura));
        legatura->switchSursa = idSwitch1;
        legatura->switchDestinatie = idSwitch2;
        legatura->portSursa = port1;
        legatura->portDestinatieEsteRootPort = 0; // LA inceput portul sursa nu este root port (niciunul)
        legatura->portDestinatie = port2;
        legatura->viteza = viteza;
        theTopology->legaturi[theTopology->numarLegaturi-1] = legatura;
        // printf("ID sw1: %d, ID Sw2: %d, port 1: %d, port 2: %d, viteza: %d\n", idSwitch1, idSwitch2, port1, port2, cost);
    }

    fclose(fp);
    return theTopology;
}


/**
 Citeste Initialize.in si preia informatiile in memorie 
*/
struct Network * readInitialize(){
    struct Network * theNet = (struct Network *)malloc(sizeof(struct Network));
    FILE * pf = fopen("Initialize.in", "r");
    int nrSwitchuri;
    fscanf(pf, "%d", &nrSwitchuri);
    
    
    theNet->switches = (struct Switch **) malloc(sizeof (struct Switch *) * nrSwitchuri);
    int i;
    for(i=0; i<nrSwitchuri; i++){
        theNet->switches[i] = (struct Switch *) malloc(sizeof (struct Switch) * nrSwitchuri);
    }
    theNet->numberOfSwitches = nrSwitchuri;
    
    for(i=0; i<nrSwitchuri; i++){
        int id, prioritate;
        char * MACtemp = (char *)malloc(sizeof(char)*20); // 20 lungimea unui MAC + 1
        fscanf(pf, "%d", &id);
        fscanf(pf, "%d", &prioritate);
        fscanf(pf, "%s", MACtemp);
        
        struct Switch * sw = (struct Switch *) malloc(sizeof(struct Switch));
        sw->id = id;
        sw->prioritate = prioritate;
        
        sw->MAC = (char *)malloc(strlen(MACtemp)*sizeof(char)+1);
        strcpy(sw->MAC, MACtemp);
        theNet->switches[i] = sw;
    }
    return theNet;
}

void mesajSwitchuri(int sursa, int destinatie){
    
}

// --- functii pentru afisare (DEBUG)
void printSwitch(struct Switch * sw){
    printf("[%d][%d][%s]\n", sw->id, sw->prioritate, sw->MAC);
}

void printTheNet(struct Network * net){
    printf("Number of switches: %d\n", net->numberOfSwitches);
    int i;
    for(i=0; i<net->numberOfSwitches; i++){
        printSwitch(net->switches[i]);
    }
}

void printLegatura(struct Legatura * leg){
    printf("[%d][%d][%d][%d][%d] RP? [%d] DP? [%d]\n", leg->switchSursa, leg->switchDestinatie, leg->portSursa, leg->portDestinatie, calculeazaCost(leg->viteza), leg->portDestinatieEsteRootPort, leg->portSursaEsteDesignatedPort);
}

void printTopologie(struct Topologie * top){
    printf("Numar legaturi: %d\n", top->numarLegaturi);
    printf("Este indicat costul, nu viteza\n");
    int i;
    for(i=0; i<top->numarLegaturi; i++){
        printLegatura(top->legaturi[i]);
    }
}

/*
void printLegatura23(struct Legatura * leg){
    printf("RP? [%d]\n", leg->portDestinatieEsteRootPort);

}

void printTopologieCERINTA23(struct Topologie * top){
    printf("Numar legaturi: %d\n", top->numarLegaturi);
    printf("Este indicat costul, nu viteza\n");
    int i;
    for(i=0; i<top->numarLegaturi; i++){
        printLegatura23(top->legaturi[i]);
    }
}

*/